package services;

import data.DataHandler;
import models.Student;
import server.Server;

import java.util.List;

public class StudentsService {
    public static boolean loginIfExists(String studentId) {
        Student student = DataHandler.getStudent(studentId);
        if (student != null) {
            Server.server.logInStudent(student);
            return true;
        }
        return false;
    }

    public static void logout() {
        Server.server.logOutStudent();
    }
}
