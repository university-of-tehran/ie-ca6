package models;


public class Student {
    public String id;
    public String name;
    public String secondName;
    public String birthDate;
    public String field;
    public String faculty;
    public String level;
    public String status;
    public String img;

    public Student(
            String id, String name, String secondName,
            String birthDate, String field, String faculty,
            String level, String status, String img) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.birthDate = birthDate;
        this.field = field;
        this.faculty = faculty;
        this.level = level;
        this.status = status;
        this.img = img;
    }
}
