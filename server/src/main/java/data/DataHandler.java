package data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import models.Course;
import models.Grade;
import models.Schedule;
import models.Student;
import org.jsoup.nodes.Document;

import repository.*;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DataHandler {
    public static List<Course> getAllCourses() {
        List<Course> courses = new ArrayList<>();
        try {
            courses = CourseRepository.getInstance().findAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return courses;
    }

    public static Course getCourse(String code) {
        try {
            return CourseRepository.getInstance().findByCode(code);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public static List<Student> getAllStudents() {
        List<Student> students = new ArrayList<>();
        try {
            students = StudentRepository.getInstance().findAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return students;
    }

    public static Student getStudent(String id) {
        try {
            return StudentRepository.getInstance().findById(id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    public static List<Grade> getStudentGrades(String studentId) {
        List<Grade> grades = new ArrayList<>();
        try {
            grades = GradeRepository.getInstance().findByStudentId(studentId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return grades;
    }

    public static List<Schedule> getPlanScheduleForStudent(String studentId) {
        List<Schedule> studentSchedules = new ArrayList<>();

        try {
            studentSchedules = ScheduleRepository.getInstance().findByStudentId(studentId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return studentSchedules;
    }

    public static List<Schedule> getAllSchedules() {
        List<Schedule> schedules = new ArrayList<>();

        try {
            schedules = ScheduleRepository.getInstance().findAll();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return schedules;
    }

    public static void insertCoursesDocToDB(Document doc) {
        ExamTimeRepository.getInstance();
        ClassTimeRepository.getInstance();

        CourseRepository courseRepository = CourseRepository.getInstance();
    
        List<Course> courses = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Type courseListType = new TypeToken<ArrayList<Course>>(){}.getType();
        courses = gson.fromJson(doc.text(), courseListType);

        for (Course course: courses) {
            try {
                courseRepository.insert(course);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void insertStudentsDocToDB(Document doc) {
        StudentRepository repository = StudentRepository.getInstance();
    
        List<Student> students = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Type studentListType = new TypeToken<ArrayList<Student>>(){}.getType();
        students = gson.fromJson(doc.text(), studentListType);

        for (Student student: students) {
            try {
                repository.insert(student);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void insertGradesToDB(Map<String, List<Grade>> studentIdToGradeMapping) {
        GradeRepository gradeRepository = GradeRepository.getInstance();
        for (Map.Entry<String, List<Grade>> entry : studentIdToGradeMapping.entrySet()) {
            String studentId = entry.getKey();
            for (Grade grade: entry.getValue()) {
                grade.studentId = studentId;
                try {
                    gradeRepository.insert(grade);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public static void writeSchedulesToDB(List<Schedule> schedules) {
        for (Schedule schedule: schedules) {
            try {
                ScheduleRepository.getInstance().insert(schedule);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static int getCourseSignedUpCount(String code, String classCode) {
        try {
            return ScheduleRepository.getInstance().getCourseSignedUpCount(code, classCode);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return 0;
    }

    public static void deleteSchedule(String studentId, String code, String classCode) {
        try {
            ScheduleRepository.getInstance().deleteSchedule(studentId, code, classCode);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}