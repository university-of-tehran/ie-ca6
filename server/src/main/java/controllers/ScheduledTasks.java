package controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import server.Server;

@Component
public class ScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 1000 * 60 * 15)
	public void submitWaitingCourses() {
        log.info("Waiting courses submitted! - Time: {}", dateFormat.format(new Date()));
    
        Server.server.submitWaitingCourses();
	}
}
